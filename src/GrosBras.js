import React from "react";

import { withStyles } from "@material-ui/core/styles";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";

import GrosBrasimage from "./assets/gros_bras_master.jpg";

const styles = (theme) => ({
  card: {
    maxWidth: 400,
    marginTop: '10%'
  },
  media: {
    height: 0,
    paddingTop: "110.25%",
  },
});

class GrosBras extends React.Component {
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState((state) => ({ expanded: !state.expanded }));
  };

  render() {
    const { classes } = this.props;

    return (
      <Card className={classes.card}>
        <CardMedia className={classes.media} image={GrosBrasimage} title="Gros Bras Master" />
      </Card>
    );
  }
}

export default withStyles(styles)(GrosBras);