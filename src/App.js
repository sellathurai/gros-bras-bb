import React, { useState } from "react";

import InfinityGauntlet from "react-thanos-snap";
import { Helmet } from "react-helmet";

import GrosBras from "./GrosBras";
import logo from "./assets/snap-logo.png";

import "./App.css";

function App() {
  const [snap, setSnap] = useState(false);

  return (
    <div id="app">
      <Helmet>
        <title>Détenteur de gros bras</title>
      </Helmet>
      <div className="wrapper">
        <InfinityGauntlet snap={snap}>
          <GrosBras />
        </InfinityGauntlet>
      </div>
      <div className="snapButton">
        <button onClick={() => setSnap(!snap)}>
          <img src={logo} alt="snap it" />
        </button>
      </div>
    </div>
  );
}

export default App;
